-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SPUPDATE
	@identificacion int,
	@nombre_cliente varchar(20),
	@Tipo_identificación varchar(20),
	@Direccion varchar(20),
	@Telefono int,
	@Correo_electronico varchar(20),
	@fecha_de_creacion date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE crudcreate1 SET @identificacion='?',@nombre_cliente='?',@Tipo_identificación='?',@Direccion='?',@Telefono='?',@Correo_electronico='?',@fecha_de_creacion='?' WHERE identificacion=@identificacion;
END
GO
