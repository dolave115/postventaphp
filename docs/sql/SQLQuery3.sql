USE [producto]
GO
/****** Object:  StoredProcedure [dbo].[SPINSERT]    Script Date: 12/09/2022 12:21:15 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SPINSERT] (,"DIEGO","cc","KR10",31222,"diego@gmail.com","2022-10-09")
	-- Add the parameters for the stored procedure here
	@Id int,
	@nombre_cliente varchar(20),
	@Tipo_identificación varchar(20),
	@Direccion varchar(20),
	@Telefono int,
	@Correo_electronico varchar(20),
	@fecha_de_creacion date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO crudcreate(id,nombre_cliente,tipo_documento,direccion,telefono,correo,fecha_creacion)
	VALUES(
	@Id,
	@nombre_cliente,
	@Tipo_identificación,
	@Direccion ,
	@Telefono,
	@Correo_electronico,
	@fecha_de_creacion );
	
END
