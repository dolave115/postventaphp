<?php
class Compañia extends Conectar{
    /* TODO: Listar Registros */
    public function get_compañia(){
        $conectar=parent::Conexion();
        $sql="SP_L_COMPAÑIA_01";
        $query=$conectar->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    /* TODO: Listar Registros */
    public function get_compañia_x_com_id($com_id){
        $conectar=parent::Conexion();
        $sql="SP_L_COMPAÑIA_02 ?";
        $query=$conectar->prepare($sql);
        $query->bindValue(1,$com_id);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    /* TODO: Eliminar registros o cambiar Registros */
    public function delete_compañia($com_id){
        $conectar=parent::Conexion();
        $sql="SP_D_COMPAÑIA_01 ?";
        $query=$conectar->prepare($sql);
        $query->bindValue(1,$com_id);
        $query->execute();
        
    }
    /* TODO: insertar registros */
    public function insert_compañia($com_nom){
        $conectar=parent::Conexion();
        $sql="SP_I_COMPAÑIA_01 ?";
        $query=$conectar->prepare($sql);
    
        $query->bindValue(1,$com_nom);
        $query->execute();
    
    } 
    /* TODO: Actualizar registros */
    public function update_compañia($com_id,$com_nom){
        $conectar=parent::Conexion();
        $sql="SP_U_COMPAÑIA_01 ?,?";
        $query=$conectar->prepare($sql);
        $query->bindValue(1,$com_id);
      
        $query->bindValue(2,$com_nom);
        $query->execute();
   
    }
}
?>