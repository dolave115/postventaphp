# Sistema de Postventa PHP,SQL SERVER y JS

_Sistema desarrollado por Diego Olave a modo de practica y material educativo_

## Comenzando 🚀

_Estas instrucciones te permitirán ejecutar el código y dar una idea básica sobre su funcionamiento._

### Pre-requisitos 📋

_Para poder inicializar el sistema se requiere lo siguiente :_

```
1. MSSQL.
2. XAMPP.
3. PHP 8 (Incluido en XAMPP).
4. Apache (Incluido en XAMPP).
5. Visual Studio Code (Editor de Codigo).
```

### Instalación 🔧
```
_Colocar la carpeta en la instalacion de XAMPP C:\xampp\htdocs_

```

## Despliegue 📦

_Copiar la carpeta del proyecto C:\xampp\htdocs, y cambiar la cadena de conexion de la base de datos en el siguiente archivo /config/conexion.php._

## Construido con 🛠️

_Las herramientas utilizadas son las siguientes_

* [PHP](http://www.php.net/) - BackEnd
* [MSSQL](https://www.microsoft.com/es-es/sql-server/sql-server-downloads) - Base de Datos
* [Visual Studio Code](https://code.visualstudio.com/) - Editor de Codigo
* [JS](https://www.javascript.com/) - FrontEnd
* [Git](https://git-scm.com/) - Manejador de Versiones
* [HTML5](https://html5.org/) - FrontEnd




## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://gitlab.com/dolave115/personalcompraventa)

## Autores ✒️

* **Diego Olave** 

---
⌨️ con ❤️ por [Anders87X](https://gitlab.com/dolave115) 😊
