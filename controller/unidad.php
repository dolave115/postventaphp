<?php
/* TODO: Llamando clases */
require_once("../config/conexion.php");
require_once("../models/Unidad.php");
/* TODO:Inicializando clase de Unidad dentro del modelo */
$unidad=new Unidad();
switch ($_GET["op"]) {
        /* TODO: Guardar y editar. guardar cuando el ID esté vacio y actualizar cuando se envie el ID */
        case "guardaryeditar":
            # code...
            if(empty($_POST["und_id"])){
                $unidad->insert_unidad($_POST["suc_id"],$_POST["und_nom"]);

            }else {
                # code...
                $unidad->update_unidad($_POST["und_id"],$_POST["suc_id"],$_POST["und_nom"]);
            }
            break;
        /* TODO: listado de registros formato json para datatable JS(frotn) */
        case "listar":
            
            # code...
            $datos=$unidad->get_unidad_x_suc_id($_POST["suc_id"]);
            $data=Array();
            foreach ($datos as $row) {
                # code...
                $sub_array=array();
                $sub_array[] = $row["UND_NOM"];
                $sub_array[] = $row["FECH_CREA"];
                $sub_array[] = '<button type="button" onClick="editar('.$row["UND_ID"].')" id="'.$row["UND_ID"].'" class="btn btn-warning btn-icon waves-effect waves-light"><i class="ri-edit-2-line"></i></button>';
                $sub_array[] = '<button type="button" onClick="eliminar('.$row["UND_ID"].')" id="'.$row["UND_ID"].'" class="btn btn-danger btn-icon waves-effect waves-light"><i class="ri-delete-bin-5-line"></i></button>';
                $data[] = $sub_array;
            }
            
            $results=array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDispayRecords"=>count($data),
                "aaData"=>$data);

            echo json_encode($results);
             break;
        /* TODO: Mostrar informacioón de registro segun su ID */
        case "mostrar":
            # code...
            $datos=$unidad->get_unidad_x_und_id($_POST["und_id"]);
            if (is_array($datos)==true and count($datos)>0) {
                # code...
                foreach($datos as $row){
                    $output["UND_ID"]=$row["UND_ID"];
                    $output["SUC_ID"]=$row["SUC_ID"];
                    $output["UND_NOM"]=$row["UND_NOM"];
                }
                echo json_encode($output);
            }
        break;
        /* TODO: Cambiar Estado a 0 del Registro(Es decir se elimina, ya que para ser activo se necesita un 1 en estado) */
        case "eliminar":
                # code...
                $unidad->delete_unidad($_POST["und_id"]);
            break;
        /* TODO: Listar Combo */
        case "combo";
        $datos=$unidad->get_unidad_x_suc_id($_POST["suc_id"]);
        if(is_array($datos)==true and count($datos)>0){
            $html="";
            $html.="<option selected>Seleccionar</option>";
            foreach($datos as $row){
                $html.= "<option value='".$row["UND_ID"]."'>".$row["UND_NOM"]."</option>";
            }
            echo $html;
        }
        break;
}

?>