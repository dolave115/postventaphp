<?php
/* TODO: Llamando clases */
require_once("../config/conexion.php");
require_once("../models/Compañia.php");
/* TODO:Inicializando clase de Compañia dentro del modelo */
$compañia=new Compañia();
switch ($_GET["op"]) {
    /* TODO: Guardar y editar. guardar cuando el ID esté vacio y actualizar cuando se envie el ID */
    case "guardaryeditar":
        # code...
        if(empty($_POST["com_id"])){
            $compañia->insert_compañia($_POST["com_nom"]);

        }else {
            # code...
            $compañia->update_compañia($_POST["com_id"],$_POST["com_nom"]);
        }
        break;
    /* TODO: listado de registros formato json para datatable JS(frotn) */
    case "listar":
        # code...
        $datos=$compañia->get_compañia($_POST["suc_id"]);
        $data=Array();
        foreach ($datos as $row) {
            # code...
            $sub_array=array();
            $sub_array=$row["com_nom"];
            $sub_array="Editar";
            $sub_array="Eliminar";
            $data[]=$sub_array;
        }
        $results=array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDispayRecords"=>count($data),
            "aaData"=>$data);

        echo json_encode($results);
        break;
    /* TODO: Mostrar informacioón de registro segun su ID */
    case "mostrar":
        # code...
        $datos=$compañia->get_compañia($_POST["com_id"]);
        if (is_array($datos)==true and count($datos)>0) {
            # code...
            foreach($datos as $row){
                $output["com_id"]=$row["com_id"];
               
                $output["com_nom"]=$row["com_nom"];
            }
            echo json_encode($output);
        }
        break;
    /* TODO: Cambiar Estado a 0 del Registro(Es decir se elimina, ya que para ser activo se necesita un 1 en estado) */
    case "eliminar":
            # code...
            $compañia->delete_compañia($_POST["com_id"]);
        break;
}

?>