<?php
/* TODO: Llamando clases */
require_once("../config/conexion.php");
require_once("../models/Moneda.php");
/* TODO:Inicializando clase de Moneda dentro del modelo */
$moneda=new Moneda();
switch ($_GET["op"]) {
    /* TODO: Guardar y editar. guardar cuando el ID esté vacio y actualizar cuando se envie el ID */
    case "guardaryeditar":
        # code...
        if(empty($_POST["mon_id"])){
            $moneda->insert_moneda($_POST["suc_id"],$_POST["mon_nom"]);

        }else {
            # code...
            $moneda->update_moneda($_POST["mon_id"],$_POST["suc_id"],$_POST["mon_nom"]);
        }
        break;
    /* TODO: listado de registros formato json para datatable JS(frotn) */
    case "listar":
        # code...
        $datos=$moneda->get_moneda_x_suc_id($_POST["suc_id"]);
        $data=Array();
        foreach ($datos as $row) {
            # code...
            $sub_array=array();
            $sub_array[]=$row["MON_NOM"];
            $sub_array[]=$row["FECH_CREA"];
            $sub_array[] = '<button type="button" onClick="editar('.$row["MON_ID"].')" id="'.$row["MON_ID"].'" class="btn btn-warning btn-icon waves-effect waves-light"><i class="ri-edit-2-line"></i></button>';
            $sub_array[] = '<button type="button" onClick="eliminar('.$row["MON_ID"].')" id="'.$row["MON_ID"].'" class="btn btn-danger btn-icon waves-effect waves-light"><i class="ri-delete-bin-5-line"></i></button>';
            $data[]=$sub_array;
        }
        $results=array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDispayRecords"=>count($data),
            "aaData"=>$data);

        echo json_encode($results);
        break;
    /* TODO: Mostrar informacioón de registro segun su ID */
    case "mostrar":
        # code...
        $datos=$moneda->get_moneda_x_mon_id($_POST["mon_id"]);
        if (is_array($datos)==true and count($datos)>0) {
            # code...
            foreach($datos as $row){
                $output["MON_ID"]=$row["MON_ID"];
                $output["SUC_ID"]=$row["SUC_ID"];
                $output["MON_NOM"]=$row["MON_NOM"];
            }
            echo json_encode($output);
        }
        break;
    /* TODO: Cambiar Estado a 0 del Registro(Es decir se elimina, ya que para ser activo se necesita un 1 en estado) */
    case "eliminar":
            # code...
            $moneda->delete_moneda($_POST["mon_id"]);
        break;
    case "combo";
    $datos=$moneda->get_moneda_x_suc_id($_POST["suc_id"]);
    if(is_array($datos)==true and count($datos)>0){
        $html="";
        $html.="<option value='0' selected>Seleccionar</option>";
        foreach($datos as $row){
            $html.= "<option value='".$row["MON_ID"]."'>".$row["MON_NOM"]."</option>";
        }
        echo $html;
    }
    break;
}




?>