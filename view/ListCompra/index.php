<?php 
 require_once("../../config/conexion.php");
 require_once("../../models/Rol.php");
 $rol = new Rol();
 $datos = $rol->validar_acceso_rol($_SESSION["USU_ID"],"listcompra");
 if(isset($_SESSION["USU_ID"])){
     if(is_array($datos) and count($datos)>0){
?>
<!doctype html>
<html lang="es" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none">

<head>

   
    <title>BitalProg |Listado de Compras</title>
    <?php require_once("../html/head.php");?>
    
</head>

<body>

    <!-- Begin page -->
    <div id="layout-wrapper">
    <?php require_once("../html/header.php");?>
      
        <!-- ========== App Menu ========== -->
        <?php require_once("../html/menu.php");?>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Listado de compras</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Listado </a></li>
                                        <li class="breadcrumb-item active">Compras</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12">
                            <div class="card">
                               
                                <div class="card-body">
                                    <table  align-items-center id="table_data" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                                        <thead>
                                            <tr>
                                                
                                                <th>Nro</th>
                                                <th>Doc.</th>
                                                <th>RUC</th>
                                                <th>Proveedor</th>
                                                
                                                <th>Pago</th>
                                                <th>Moneda</th>
                                                <th>Subtotal</th>
                                                <th>IVA</th>
                                                <th>Total</th>
                                                <th>Usuario</th>
                                                <th></th>
                                                <th>Imprimir</th>
                                                <th>Ver</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div>
                    <!-- end page title -->

                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->

            <?php require_once("../html/footer.php");?>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->
    <?php require_once("mantenimiento.php");?>


    <!--start back-to-top-->
    <?php require_once("../html/js.php");?>
    <<script type="text/javascript" src="listcompra.js">
        
    </script>
</body>

</html>
<?php
      }else{
        header("Location:".Conectar::ruta()."view/404/");
    }
}else{
    header("Location:".Conectar::ruta()."view/404/");
}
?>