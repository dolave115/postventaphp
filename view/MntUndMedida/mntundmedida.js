var suc_id = $('#SUC_IDx').val();

function init(){
    $("#mantenimiento_form").on("submit",function(e){
        guardaryeditar(e);
    });
}
function guardaryeditar(e){
    e.preventDefault();
    var formData = new FormData($("#mantenimiento_form")[0]);
    formData.append('suc_id',$('#SUC_IDx').val());
    $.ajax({
        url:"../../controller/unidad.php?op=guardaryeditar",
        type:"POST",
        data:formData,
        contentType:false,
        processData:false,
        success:function(data){
            $('#table_data').DataTable().ajax.reload();
            $('#modalmantenimiento').modal('hide');
            swal.fire({
                title:'Unidad de mantenimiento',
                text:'Registro confirmado',
                icon: 'success'
                        });
                console.log(data);
        }
    });
}
$(document).ready(function(){
    /* TODO: Listar informacion en el datatable js */
    $('#table_data').DataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
        ],
        "ajax":{
            url:"../../controller/unidad.php?op=listar",
            type:"post",
            data:{suc_id:suc_id}
        },
        "bDestroy": true,
        "responsive": true,
        "bInfo":true,
        "iDisplayLength": 10,
        "order": [[ 0, "desc" ]],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

});

function editar(und_id){
    console.log(und_id);
    $.post("../../controller/unidad.php?op=mostrar",{und_id:und_id},function(data){
        data=JSON.parse(data);
        $('#und_id').val(data.UND_ID);
        $('#und_nom').val(data.UND_NOM);
        console.log(data);
    });
    $('#lbltitulo').html('Editar Registro');
    /* TODO: Mostrar Modal */
    $('#modalmantenimiento').modal('show');
    
}

function eliminar(und_id){
    console.log(und_id);
    Swal.fire({
        title: 'ELIMINAR!',
        text: "Desea eliminar el registro?",
        icon: 'warning',
        confirmButtonText : "Si",
        showCancelButton: true,
        confirmButtonColor: '#186823',
        cancelButtonColor: '#683c18',
        cancelButtonText: "No",
      }).then((result) => {
        if (result.isConfirmed) {
            $.post("../../controller/unidad.php?op=eliminar",{und_id:und_id},function(data){
                console.log(data);
            });
            $('#table_data').DataTable().ajax.reload();
            Swal.fire({
                title:'Unidad de medida',
                text: 'Registro Eliminado',
                icon: 'success'
            });
       
        }
      })
    
}
$(document).on("click","#btnnuevo",function(){
    $('#und_id').val('');
    $('#und_nom').val('');
    $('#lbtitulo').html('Nuevo Registro');
    $("#mantenimiento_form")[0].reset();
    $('#modalmantenimiento').modal('show');

});
init();