<?php 
require_once("../../config/conexion.php");
if(isset($_SESSION["USU_ID"])){

?>
<!doctype html>
<html lang="es" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none">

<head>

   
    <title>BitalProg | Rol</title>
    <?php require_once("../html/head.php");?>
    
</head>

<body>

    <!-- Begin page -->
    <div id="layout-wrapper">
    <?php require_once("../html/header.php");?>
      
        <!-- ========== App Menu ========== -->
        <?php require_once("../html/menu.php");?>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Mantenimiento Rol</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Mantenimiento </a></li>
                                        <li class="breadcrumb-item active">Rol</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                <button type="button" id="btnnuevo" class="btn btn-primary btn-label waves-effect right waves-light"><i class="ri-user-smile-line label-icon align-middle fs-16 ms-2"></i> NUEVO REGISTRO</button>
                                </div>
                                <div class="card-body">
                                    <table id="table_data" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                                        <thead>
                                            <tr>
                                                
                                                
                                                <th>Nombre</th>
                                                <th>Fecha Creacion</th>
                                                <th>Permisos</th>
                                                <th>Editar</th>
                                                <th>Eliminar</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div>
                    <!-- end page title -->

                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->

            <?php require_once("../html/footer.php");?>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    <?php require_once("mantenimiento.php");?>
    <?php require_once("modalpermiso.php");?>
    <!--start back-to-top-->
    <?php require_once("../html/js.php");?>
    <<script type="text/javascript" src="mntrol.js">
        
    </script>
</body>

</html>
<?php
      }else{
        header("Location:".Conectar::ruta()."view/404/");
    }
?>